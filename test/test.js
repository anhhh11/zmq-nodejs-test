var chai = require('chai')
,sinon = require('sinon')
,expect = chai.expect
,sock
,zmq = require('zmq')
,spawn = require('child_process').spawn
,producer
,sandbox;
describe('Socket',function(){
  before(function(){
    producer = spawn('node',[__dirname + '/../producer.js']);
    producer.stdout.on('data',function(msg){
      // console.log('[OUT] '+msg);
    });
    producer.stderr.on('data',function(msg){
      // console.log.bind('[ERR] '+msg);
    });
    producer.on('close',function(msg){
      // console.log('[CLOSE] '+msg);
    });
  });
  after(function(){
    producer.kill('SIGKILL');
  });
  beforeEach(function(){
    sock = zmq.socket('pull');
    sock.connect('tcp://127.0.0.1:3001');
  });
  afterEach(function(){
    try {
      sock.close();
    } catch(e) {
    }
  });
  it('should receive work',function(done){
      sock.on('message',function(message){
      expect(message.toString()).to.match(/some work [1-9]*/);
      // console.info('received message') ;
      sock.close();
      done();
    });

});

})
describe('HelloWorld',function(){
  beforeEach(function(){
    sandbox = sinon.sandbox.create();
    sandbox.stub(console,"log");
  });
  afterEach(function(){
    sandbox.restore();
  })

  it('should receive hello when sending hello',function(){
    expect(2).to.equal(2);
  });
  it('should call log to console',function(){
    console.log('world');
    sinon.assert.calledWithExactly(console.log,'world');
  });
  it('should return `word` after 2 second',function(done){
    setTimeout(function(){
      console.log('hello');
      done();
    },100);
  });
});
